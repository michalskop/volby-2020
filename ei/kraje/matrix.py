"""Create pictures/matrices from EI."""

import csv
import icu
import json
import locale
import math
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

path = "/home/michal/dev/prechody/volby-2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

mec = election_codes[0] # 'kraje2020' # main_election_code
election_codes = election_codes[1:] # ['psp2017', 'pres2018-1', 'pres2018-2', 'euro2019', 'kraje2016'] # additionals

collator = icu.Collator.createInstance(icu.Locale('cs_CZ.UTF-8'))

# list of regions
region_codes = []
regions = {}
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])
        regions[row['code']] = row

# elections
with open(path + "elections.json") as fin:
    elections = json.load(fin)

# ec = 'kraje2016'
# code = 'CZ080'
for ec in election_codes:
    print(ec)
    for code in region_codes:

        # colors
        mparties = {}
        with open(path + "data/list_" + code + "_" + mec + "_filtered.csv") as fin:
            dr = csv.DictReader(fin)
            for row in dr:
                mparties[row['abbreviation']] = row

        # MATRIX / DATA

        # reorder alphabetically + remove the artifical (migrants) variable
        df = pd.read_csv(path + "data/matn_" + code + "_" + mec + "_" + ec + ".csv", index_col=0)
        df.set_value('nevoliči', 'nevoliči', 0)
        df = df.drop('migrace', axis=0)
        df = df.drop('migrace', axis=1)
        if df.T.max()['ostatní'] <= 1:
            df = df.drop('ostatní', axis=0)
        df = df.rename(columns={"nevoliči": "ZZZ"})
        df = df.rename(columns={"ostatní": "ZZ"})
        df = df.rename(index={"nevoliči": "ZZZ"})
        df = df.rename(index={"ostatní": "ZZ"})
        df = df.reindex(sorted(df.columns, key=collator.getSortKey), axis=1)
        df = df.T
        df = df.reindex(sorted(df.columns, key=collator.getSortKey), axis=1)
        df = df.T
        df = df.rename(columns={"ZZZ": "nevoliči"})
        df = df.rename(columns={"ZZ": "ostatní"})
        df = df.rename(index={"ZZZ": "nevoliči"})
        df = df.rename(index={"ZZ": "ostatní"})

        def _smart_round(x):
            xx = x
            c = 0.1
            while (xx > 0):
                xx = xx // 10
                c = 10 * c
            return int(round(x / c) * c)

        # reorder for plotting
        table = []
        colors = []
        max_value = max(df.max())
        for c in df.columns:
            for r in df.index.values:
                if df[c][r] > (max_value / 4):
                    txt = '<b>' + locale.format_string('%.0f', (_smart_round(df[c][r])), True) + '</b>'
                else:
                    txt = ''
                table.append({
                    mec: c,
                    ec: r,
                    "value": df[c][r],
                    "color": mparties[c]['color'],
                    "text_static":  txt,
                    "text": '<b>' + locale.format_string('%.0f', (_smart_round(df[c][r])), True) + '</b>'
                })
            colors.append(mparties[c]['color'])
        tabledf = pd.DataFrame.from_dict(table)

        # CHARTING

        # express chart:
        # fig = px.scatter(table, 
        #     x=ec,
        #     y=mec, 
        #     size="value", 
        #     color="color", 
        #     color_discrete_sequence=colors
        # )
        # fig.show()
        sizes = [
            {"width": 500, "height": 450},
            {"width": 750, "height": 425},
            {"width": 1000, "height": 400},
            {"width": 1000, "height": 900},
            {"width": 1360,  "height": 1220},
            {"width": 680, "height": 610},
            {"width": 507, "height": 245, "footer": 40},
            {"width": 1014, "height": 490, "footer": 80}
        ]
        for s in sizes:
            width = s['width']
            height = s['height']
            sizecoefmin = (min(width, height) / 500)
            sizecoefmax = (min(width, height) / 500)
            max_bubble_size = 60 * (sizecoefmin + sizecoefmax) / 2
            fig = go.Figure(data=[go.Scatter(
                x=tabledf[ec],
                y=tabledf[mec],
                text=tabledf['text_static'],
                textposition="middle center",
                mode='markers+text',
                textfont={
                    "family": "Ubuntu",
                    "color": "#eee",
                    "size": 10 * (sizecoefmin + sizecoefmax) / 2 # math.sqrt(sizecoefmin)
                },
                marker=dict(
                    size=tabledf['value'],
                    sizemode='area',
                    sizeref=2*max_value/(max_bubble_size**2),
                    sizemin=0,
                    color=tabledf['color'],
                )
            )])

            fig.update_layout(
                title='<b>' + regions[code]['name'] + "</b><br>" + elections[ec]['name'] + " → " + elections[mec]['name'],
                title_font=dict(
                    # size=22 * math.sqrt(sizecoefmax),
                    size=22 * sizecoefmax,
                    color="#772953"
                ),
                xaxis_title=elections[ec]['name'],
                yaxis_title=elections[mec]['name'],
                xaxis_title_font=dict(
                    size=16  * sizecoefmax,
                    # size= 16 * math.sqrt(sizecoefmax),
                    color="#772953"
                ),
                yaxis_title_font=dict(
                    size=16 * sizecoefmax, # math.sqrt(sizecoefmax),
                    color="#772953"
                ),
                template='plotly_white',
                width=width,
                height=height,
                margin=dict(
                    l=40 * sizecoefmax, # math.sqrt(sizecoefmax),
                    r=0,
                    t=80 * sizecoefmax, # math.sqrt(sizecoefmax),
                    b=80 * sizecoefmax, # math.sqrt(sizecoefmax)
                ),
                # paper_bgcolor="#e95420"
            )

            # fig.update_xaxes(tickangle=45, tickfont=dict(size=12 * math.sqrt(sizecoefmax), color="#868e96"))
            # fig.update_yaxes(tickangle=-45, tickfont=dict(size=12 * math.sqrt(sizecoefmax), color="#868e96"))
            fig.update_xaxes(tickangle=45, tickfont=dict(size=12 * sizecoefmax, color="#868e96"))
            fig.update_yaxes(tickangle=-45, tickfont=dict(size=12 * sizecoefmax, color="#868e96"))

            #fig.show()
            # save images
            fig.write_image(path + "images/" + code + "_" + mec + "_" + ec + "_" + str(width) + "_" + str(height) + ".png")
            fig.write_image(path + "images/" + code + "_" + mec + "_" + ec + "_" + str(width) + "_" + str(height) + ".svg")

        # change texts, save html
        fig['data'][0]['text'] = tabledf['text']
        fig['data'][0]['mode'] = 'markers'
        fig.update_layout(
            height=500,
            width=None
        )

        fig.write_html(path + "images/" + code + "_" + mec + "_" + ec + "_500.html", include_plotlyjs="cdn", full_html=False, default_width="100%")
        # df

collator = icu.Collator.createInstance(icu.Locale('en_US.UTF-8'))
print("done")