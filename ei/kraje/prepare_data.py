"""Prepare data for EI."""

import csv
# import io
import json
# import requests

data_path = "/home/michal/dev/volebniatlas_backend/geo_data/region/"
path = "/home/michal/dev/prechody/volby-2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

# list of regions
region_codes = []
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])

# party colors list:
# p2color = {}
# url = "https://raw.githubusercontent.com/michalskop/political_parties/master/cz/parties.csv"
# r = requests.get(url)
# buff = io.StringIO(r.text)
# dr = csv.DictReader(buff)
# for row in dr:
#     p2color[row['volby.cz']] = row

for code in region_codes:
#code = "CZ032"

    with open(data_path + "region_" + code + ".json") as fin:
        geo_data = json.load(fin)

    for ec in election_codes:
        # data
        header = ['OBEC', 'OKRSEK', 'VOLICI', 'HLASY']
        rows = []
        for station in geo_data['objects']['tracts']['geometries']:
            if 'statistics' in station['properties']['data'][ec].keys():
                item = {
                    'OBEC': station['properties']['place_id'],
                    'OKRSEK': station['properties']['station_cod'],
                    'VOLICI': station['properties']['data'][ec]['statistics']['total_voters'],
                    'HLASY': station['properties']['data'][ec]['statistics']['valid_votes']
                }
                for p in station['properties']['data'][ec]['results']:
                    if ('HLASY_' + str(p)) not in header:
                        header.append('HLASY_' + str(p))
                    item['HLASY_' + str(p)] = station['properties']['data'][ec]['results'][p]
                rows.append(item)

        with open(path + "data/data_" + code + "_" + ec + ".csv", "w") as fout:
            dw = csv.DictWriter(fout, header)
            dw.writeheader()
            for row in rows:
                dw.writerow(row)

        # list filtered
        header = ["id", "name", "abbreviation", "color", "votes"]
        rows = []
        for p in geo_data['data'][ec]['results']:
            if geo_data['data'][ec]['results'][p] / geo_data['data'][ec]['statistics']['valid_votes'] >= 0.05:
                item = geo_data['candidates'][ec][p]
                item['votes'] = geo_data['data'][ec]['results'][p]
                rows.append(item)
        
        additinals = ["ostatní", "migrace", "nevoliči"]
        color = 666
        for a in additinals:
            item = {
                "id": "",
                "name": "",
                "abbreviation": a,
                "color": "#" + str(color),
                "votes": ""
            }
            rows.append(item)
            color -= 222
        
        with open(path + "data/list_" + code + "_" + ec + "_filtered.csv", "w") as fout:
            dw = csv.DictWriter(fout, header)
            dw.writeheader()
            for row in rows:
                dw.writerow(row)
            