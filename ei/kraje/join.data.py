"""Join the data for EI."""
import csv
import json
import random

path = "/home/michal/dev/prechody/volby-2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

mec = election_codes[0] # 'kraje2020' # main_election_code
election_codes = election_codes[1:] # ['psp2017', 'pres2018-1', 'pres2018-2', 'euro2019', 'kraje2016'] # additionals
select = 0.9 # how many okrseks should be selected

def _n(s):
    if s == '':
        return 0
    else:
        return int(s)

# list of regions
region_codes = []
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])

for code in region_codes:
    for ec in election_codes:

        # ec = 'kraje2016'
        # code = "CZ032"

        previous = code + "_" + ec
        current = code + "_" + mec



        # data rows
        d = {
            previous: {},
            current: {}
        }
        # stations in municipality for matching in changed numbering municipality
        stations = {
            previous: {},
            current: {}
        }
        # parties filteres
        parties = {
            current: [],
            previous: []
        }
        # selected data, filtered out 6 % of polling stations
        data = {}
        for y in d:
            with open(path + "data/data_" + y + ".csv") as fin:
                dr = csv.DictReader(fin)
                for row in dr:
                    try:
                        stations[y][row['OBEC']]
                    except Exception:
                        stations[y][row['OBEC']] = []
                        d[y][row['OBEC']] = {}
                    stations[y][row['OBEC']].append(row['OKRSEK'])
                    d[y][row['OBEC']][row['OKRSEK']] = row


            with open(path + "data/list_" + y + "_filtered.csv") as fin:
                dr = csv.DictReader(fin)
                for row in dr:
                    if row['id'] != '':
                        parties[y].append(row['id'])

        for muni in d[current]:
            same = True
            for okr in stations[current][muni]:
                try:
                    d[previous][muni][okr]
                except Exception:
                    same = False
            if same:
                data[muni] = {}
                for okr in stations[current][muni]:
                    data[muni][okr] = {}
                    data[muni][okr][previous] = d[previous][muni][okr]
                    data[muni][okr][current] = d[current][muni][okr]
            else:
                try:
                    if len(stations[previous][muni]) == len(stations[current][muni]):
                        data[muni] = {}
                        for i in range(0, len(stations[previous][muni])):
                            o_current = stations[current][muni][i]
                            o_previous = stations[previous][muni][i]
                            data[muni][o_current] = {}
                            data[muni][o_current][previous] = d[previous][muni][o_previous]
                            data[muni][o_current][current] = d[current][muni][o_current]
                    else:
                        print(muni, len(stations[current][muni]), '::not the same number or stations')
                except Exception:
                    print(muni, len(stations[current][muni]), '::not in previous')

        # randomly select some districts to be able to perform EI
        with open(path + "data/data_" + code + "_" + mec + "_" + ec + "_filtered_random.csv", "w") as foutR:
            header = []
            for k in parties[previous]:
                header.append('A' + str(k))
            header.append('A997')   # ostatní strany
            header.append('A998')   # odešlí
            header.append('A999')   # nevoliči
            for k in parties[current]:
                header.append('B' + str(k))
            header.append('B997')
            header.append('B998')
            header.append('B999')
            drR = csv.DictWriter(foutR, header)
            drR.writeheader()
            with open(path + "data/data_" + code + "_" + mec + "_" + ec + "_filtered.csv", "w") as fout:
                dr = csv.DictWriter(fout, header)
                dr.writeheader()
                for muni in data:
                    for okr in data[muni]:
                        row = {}
                        for y in parties:
                            if y == previous:
                                codeAB = 'A'
                            else:
                                codeAB = 'B'
                            s = 0
                            for k in parties[y]:
                                try:
                                    num = round(float(data[muni][okr][y]['HLASY_' + k]))
                                except Exception:
                                    num = 0
                                s += num
                                row[codeAB + str(k)] = num
                            row[codeAB + '997'] = int(data[muni][okr][y]['HLASY']) - s
                            row[codeAB + '999'] = int(data[muni][okr][y]['VOLICI']) - int(data[muni][okr][y]['HLASY'])

                        if int(data[muni][okr][previous]['VOLICI']) >= int(data[muni][okr][current]['VOLICI']):
                            row['B998'] = round(float(data[muni][okr][previous]['VOLICI'])) - int(data[muni][okr][current]['VOLICI'])
                            row['A998'] = 0
                        else:
                            row['A998'] = int(data[muni][okr][current]['VOLICI']) - round(float(data[muni][okr][previous]['VOLICI']))
                            row['B998'] = 0
                        if (1.25 * int(data[muni][okr][previous]['VOLICI']) >= int(data[muni][okr][current]['VOLICI'])) and (0.8 * round(float(data[muni][okr][previous]['VOLICI'])) <= int(data[muni][okr][current]['VOLICI'])):
                            dr.writerow(row)
                            if random.random() < select:
                                drR.writerow(row)