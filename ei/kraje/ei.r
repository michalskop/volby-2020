# Ecological inference using ei package
# path = "/home/michal/dev/pl_2020/ei/"

# Ntotal = 30204241  # Voliči v seznamu

# list_previous_name = "list_2019_filtered.csv"
# list_current_name = "list_2020_1_filtered.csv"

setwd(path)

library(ei)

# Prepare data and variables
data = read.csv(data_filtered_random, header = TRUE, fileEncoding = "utf-8", encoding = "utf-8")
variables = names(data)
As = variables[which(grepl("A", variables) == TRUE)]
Bs = variables[which(grepl("B", variables) == TRUE)]

# Run EI
# Example:
# dbuf = ei(cbind(B5, B9, B26, B27, B28, B30, B39, B997, B998, B999) ~ cbind(A1, A4, A7, A8, A15, A20, A21, A24, A29, A997, A998, A999), data = data)
eval(parse(text = paste0( "dbuf = ei(cbind(" , paste(Bs, collapse = ", "), ") ~ cbind(", paste(As, collapse = ", "), "), data = data)")))

# create and save result matrices
list_previous = read.csv(list_previous_name, header = TRUE, fileEncoding = "utf-8", encoding = "utf-8")
list_current = read.csv(list_current_name, header = TRUE, fileEncoding = "utf-8", encoding = "utf-8")

nrows = dim(data)[1]
mat_ave = matrix(nrow=length(As), ncol=length(Bs), dimnames=list(As, Bs))
for (B in Bs) {
    for (A in As) {
        s = 0
        for (r in 1:nrows) {
            a = paste0("beta.", A, ".", B, ".", r)
            s = s + mean(dbuf$draws$Beta[,a]) * data[r, A]
        }
        mat_ave[A, B] = s
    }
}
rownames(mat_ave) = list_previous[, "abbreviation"]
colnames(mat_ave) = list_current[, "abbreviation"]

matp = mat_ave / apply(mat_ave,1,'sum')

tmat_ave = t(mat_ave)

write.csv(mat_ave, matn_filtered_name)
write.csv(tmat_ave, matn_chart_filtered)
write.csv(matp, matp_filtered)

n = sum(data)/2

write.csv(mat_ave * Ntotal / n, matn)
write.csv(tmat_ave * Ntotal / n, matn_chart)
